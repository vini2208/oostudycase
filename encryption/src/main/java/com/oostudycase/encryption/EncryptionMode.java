package com.oostudycase.encryption;

public interface EncryptionMode {

	public String encrypt(String valueToEncrypt);

}
