package com.studycase.validation.model;

import org.junit.Test;

import com.studycase.model.Aluno;
import com.studycase.validation.DataValidationException;

public class AlunoValidatorTest {

	@Test(expected = DataValidationException.class)
	public void testValidateNullAluno() throws DataValidationException {
		new AlunoValidator().validate(null);
	}

	@Test(expected = DataValidationException.class)
	public void testValidateAlunoInvalidCPF() throws DataValidationException {
		new AlunoValidator().validate(new Aluno("Julio Dias", "54321", 18));
	}

	@Test(expected = DataValidationException.class)
	public void testValidateAlunoInvalidNome() throws DataValidationException {
		new AlunoValidator().validate(new Aluno("   ", "123.123.123-12", 22));
	}

	@Test(expected = DataValidationException.class)
	public void testValidateAlunoInvalidAge() throws DataValidationException {
		new AlunoValidator().validate(new Aluno("Douglas Cunha", "123.123.123-12", 2));
	}

	@Test
	public void testValidateAluno() throws DataValidationException {
		new AlunoValidator().validate(new Aluno("Samanta Cruz", "123.123.123-12", 20));
	}

}
