package com.oostudycase.main;

import com.oostudycase.decryption.DecryptionMode;
import com.oostudycase.decryption.DecryptionModeEnum;
import com.oostudycase.decryption.DecryptionModeFactory;
import com.oostudycase.encryption.EncryptionMode;
import com.oostudycase.encryption.EncryptionModeEnum;
import com.oostudycase.encryption.EncryptionModeFactory;

public class UsageSample {

	public static void main(String[] args) {
		String valueToEncrypt = "Object Orientation study case";
		System.out.println("Value to encrypt: " + valueToEncrypt);

		System.out.println();

		EncryptionMode encryptionMode = EncryptionModeFactory.getEncryptionMode(EncryptionModeEnum.BASE64);
		System.out.println("Encryption implementation class: " + encryptionMode.getClass());

		String encryptedValue = encryptionMode.encrypt(valueToEncrypt);
		System.out.println("Encrypted value: " + encryptedValue);

		System.out.println();

		DecryptionMode decryptionMode = DecryptionModeFactory.getDecryptionMode(DecryptionModeEnum.BASE64);
		System.out.println("Decryption implementation class: " + decryptionMode.getClass());

		String decryptedValue = decryptionMode.decrypt(encryptedValue);
		System.out.println("Decrypted value: " + decryptedValue);
	}

}
