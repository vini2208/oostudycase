package com.studycase.validation;

public interface DataValidation<T> {

	public void validate(T object) throws DataValidationException;

}
