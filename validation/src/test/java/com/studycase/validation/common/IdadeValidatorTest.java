package com.studycase.validation.common;

import org.junit.Test;

import com.studycase.validation.DataValidationException;

public class IdadeValidatorTest {

	@Test(expected = DataValidationException.class)
	public void testValidateNullIdade() throws DataValidationException {
		new IdadeValidator().validate(null);
	}

	@Test(expected = DataValidationException.class)
	public void testValidateIdadeLessThan16() throws DataValidationException {
		new IdadeValidator().validate(14);
	}

	@Test
	public void testValidateIdade() throws DataValidationException {
		new IdadeValidator().validate(22);
	}

}
