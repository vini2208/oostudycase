package com.studycase.validation.common;

import org.junit.Test;

import com.studycase.validation.DataValidationException;

public class CPFValidatorTest {

	@Test(expected = DataValidationException.class)
	public void testValidateNullCPF() throws DataValidationException {
		new CPFValidator().validate(null);
	}

	@Test(expected = DataValidationException.class)
	public void testValidateEmptyCPF() throws DataValidationException {
		new CPFValidator().validate("");
	}

	@Test(expected = DataValidationException.class)
	public void testValidateBlankCPF() throws DataValidationException {
		new CPFValidator().validate("     ");
	}

	@Test(expected = DataValidationException.class)
	public void testValidateInvalidValueCPF() throws DataValidationException {
		new CPFValidator().validate("123456");
	}

	@Test
	public void testValidateCPF() throws DataValidationException {
		new CPFValidator().validate("123.456.789-99");
	}

}
