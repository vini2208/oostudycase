package com.studycase.validation.common;

import com.studycase.validation.DataValidation;
import com.studycase.validation.DataValidationException;

public class NomeValidator implements DataValidation<String> {

	@Override
	public void validate(String nome) throws DataValidationException {
		if (null == nome || nome.trim().isEmpty()) {
			throw new DataValidationException("Nome can't be empty!");
		}
	}

}
