package com.oostudycase.decryption;

public interface DecryptionMode {

	public String decrypt(String valueToDecrypt);

}
