package com.oostudycase.decryption;

import org.junit.Assert;
import org.junit.Test;

public class DecryptionModeFactoryTest {

	@Test(expected = IllegalArgumentException.class)
	public void testGetDecryptionModeNullArg() {
		DecryptionModeFactory.getDecryptionMode(null);
	}

	@Test
	public void testGetDecryptionModeBase64Mode() {
		DecryptionMode DecryptionMode = DecryptionModeFactory.getDecryptionMode(DecryptionModeEnum.BASE64);
		Assert.assertTrue(DecryptionMode instanceof Base64DecryptionMode);
	}

	@Test
	public void testGetDecryptionModeHomophonicMode() {
		DecryptionMode DecryptionMode = DecryptionModeFactory.getDecryptionMode(DecryptionModeEnum.HOMOPHONIC);
		Assert.assertTrue(DecryptionMode instanceof HomophonicSubstitutionDecryptionMode);
	}

}
