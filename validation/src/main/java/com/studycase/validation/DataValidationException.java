package com.studycase.validation;

public class DataValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataValidationException(String message) {
		super(message);
	}

	public DataValidationException(Throwable cause) {
		super(cause);
	}

}
