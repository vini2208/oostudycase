package com.oostudycase.decryption;

import java.util.Base64;

class Base64DecryptionMode implements DecryptionMode {

	@Override
	public String decrypt(String valueToDecrypt) {
		return new String(Base64.getDecoder().decode(valueToDecrypt));
	}

}
