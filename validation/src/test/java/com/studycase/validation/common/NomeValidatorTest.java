package com.studycase.validation.common;

import org.junit.Test;

import com.studycase.validation.DataValidationException;

public class NomeValidatorTest {

	@Test(expected = DataValidationException.class)
	public void testValidateNullNome() throws DataValidationException {
		new NomeValidator().validate(null);
	}

	@Test(expected = DataValidationException.class)
	public void testValidateEmptyNome() throws DataValidationException {
		new NomeValidator().validate("");
	}

	@Test(expected = DataValidationException.class)
	public void testValidateBlankNome() throws DataValidationException {
		new NomeValidator().validate("         ");
	}

	@Test
	public void testValidateNome() throws DataValidationException {
		new NomeValidator().validate("Julio Dias");
	}

}
