package com.oostudycase.encryption;

import org.junit.Assert;
import org.junit.Test;

public class Base64EncryptionModeTest {

	@Test
	public void testEncrypt() {
		String valueToEncrypt = "Object Orientation Study Case";
		String encryptedValueExpected = "T2JqZWN0IE9yaWVudGF0aW9uIFN0dWR5IENhc2U=";

		String encryptedValue = new Base64EncryptionMode().encrypt(valueToEncrypt);

		Assert.assertEquals(encryptedValueExpected, encryptedValue);
	}

}
