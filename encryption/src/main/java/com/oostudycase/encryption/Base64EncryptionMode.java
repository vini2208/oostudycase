package com.oostudycase.encryption;

import java.util.Base64;

class Base64EncryptionMode implements EncryptionMode {

	@Override
	public String encrypt(String valueToEncrypt) {
		return Base64.getEncoder().encodeToString(valueToEncrypt.getBytes());
	}

}
