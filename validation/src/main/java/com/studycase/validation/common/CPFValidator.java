package com.studycase.validation.common;

import com.studycase.validation.DataValidation;
import com.studycase.validation.DataValidationException;

public class CPFValidator implements DataValidation<String> {

	private static final String CPF_PATTERN = "([0-9]{3}\\.){2}[0-9]{3}\\-[0-9]{2}";

	@Override
	public void validate(String cpf) throws DataValidationException {
		if (null == cpf || cpf.trim().isEmpty()) {
			throw new DataValidationException("CPF can't be empty!");
		}

		if (!cpf.matches(CPF_PATTERN)) {
			throw new DataValidationException("CPF has an invalid value!");
		}
	}

}
