package com.oostudycase.encryption;

public class EncryptionModeFactory {

	private EncryptionModeFactory() {
	}

	public static EncryptionMode getEncryptionMode(EncryptionModeEnum encryptionModeEnum) {
		if (null == encryptionModeEnum) {
			throw new IllegalArgumentException("Encryption mode cannot be null!");
		}

		switch (encryptionModeEnum) {
		case HOMOPHONIC:
			return new HomophonicSubstitutionEncryptionMode();
		default:
			return new Base64EncryptionMode();
		}
	}

}
