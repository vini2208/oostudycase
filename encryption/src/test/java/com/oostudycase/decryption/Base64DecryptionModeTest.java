package com.oostudycase.decryption;

import org.junit.Assert;
import org.junit.Test;

public class Base64DecryptionModeTest {

	@Test
	public void testDecrypt() {
		String valueToDecrypt = "T2JqZWN0IE9yaWVudGF0aW9uIFN0dWR5IENhc2U=";
		String decryptedValueExpected = "Object Orientation Study Case";

		String decryptedValue = new Base64DecryptionMode().decrypt(valueToDecrypt);

		Assert.assertEquals(decryptedValueExpected, decryptedValue);
	}

}
