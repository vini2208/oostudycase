package com.oostudycase.encryption;

import org.junit.Assert;
import org.junit.Test;

public class EncryptionModeFactoryTest {

	@Test(expected = IllegalArgumentException.class)
	public void testGetEncryptionModeNullArg() {
		EncryptionModeFactory.getEncryptionMode(null);
	}

	@Test
	public void testGetEncryptionModeBase64Mode() {
		EncryptionMode encryptionMode = EncryptionModeFactory.getEncryptionMode(EncryptionModeEnum.BASE64);
		Assert.assertTrue(encryptionMode instanceof Base64EncryptionMode);
	}

	@Test
	public void testGetEncryptionModeHomophonicMode() {
		EncryptionMode encryptionMode = EncryptionModeFactory.getEncryptionMode(EncryptionModeEnum.HOMOPHONIC);
		Assert.assertTrue(encryptionMode instanceof HomophonicSubstitutionEncryptionMode);
	}

}
