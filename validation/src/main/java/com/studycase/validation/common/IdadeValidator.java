package com.studycase.validation.common;

import com.studycase.validation.DataValidation;
import com.studycase.validation.DataValidationException;

public class IdadeValidator implements DataValidation<Integer> {

	@Override
	public void validate(Integer idade) throws DataValidationException {
		if (null == idade) {
			throw new DataValidationException("Idade can't be null!");
		}

		if (idade < 16) {
			throw new DataValidationException("Idade can't be less than 16!");
		}
	}

}
