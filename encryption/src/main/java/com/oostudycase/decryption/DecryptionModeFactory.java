package com.oostudycase.decryption;

public class DecryptionModeFactory {

	private DecryptionModeFactory() {
	}

	public static DecryptionMode getDecryptionMode(DecryptionModeEnum decryptionModeEnum) {
		if (null == decryptionModeEnum) {
			throw new IllegalArgumentException("Decryption mode cannot be null!");
		}

		switch (decryptionModeEnum) {
		case HOMOPHONIC:
			return new HomophonicSubstitutionDecryptionMode();
		default:
			return new Base64DecryptionMode();
		}
	}

}
