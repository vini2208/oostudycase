package com.studycase.validation.model;

import com.studycase.model.Aluno;
import com.studycase.validation.DataValidation;
import com.studycase.validation.DataValidationException;
import com.studycase.validation.common.CPFValidator;
import com.studycase.validation.common.IdadeValidator;
import com.studycase.validation.common.NomeValidator;

public class AlunoValidator implements DataValidation<Aluno> {

	@Override
	public void validate(Aluno aluno) throws DataValidationException {
		if (null == aluno) {
			throw new DataValidationException("Aluno can't be null!");
		}

		new CPFValidator().validate(aluno.getCpf());
		new IdadeValidator().validate(aluno.getIdade());
		new NomeValidator().validate(aluno.getNome());
	}

}
